/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    
    //mockup
    static {
        UserService.load();
        if(userList.isEmpty()) {
            userList.add(new User("belle", "password"));
            userList.add(new User("bala", "password"));
        } 
    }
    
    //create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }
    
    public static boolean addUser(ArrayList<User> user) {
        userList.addAll(user);
        return true;
    }    
    
    public static boolean addUser(String username , String password) {
        userList.add(new User(username , password));
        return true;
    }    
    
    //update
    public static boolean updateUser(int index , User user) {
        userList.set(index , user);
        return true;
    }
    
    //Read 1 User
    public static User getUser(int index) {
        if(index > userList.size()-1) {
            return null;
        }
        return userList.get(index);
    }
    
    public static ArrayList<User> getUsers() {
        return userList;
    }
     
        //search username
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for(User user : userList) {
            if(user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }
    
    //delete user
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }
    
      public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }  
      
      //login
      public static User login(String username , String password) {
          for(User user : userList) {
              if(user.getUserName().equals(username) && user.getPassword().equals(password)) {
                  return user;
              }
          }
          return null;
      }
      
      public static void save() {
          FileOutputStream fos = null;
          try {
              File file = new File("User.dat");
              
              fos = new FileOutputStream(file);
              ObjectOutputStream oos = new ObjectOutputStream(fos);
              oos.writeObject(UserService.getUsers());
              oos.close();
              fos.close();
              
          } catch (FileNotFoundException ex) { 
            
        } catch (IOException ex) {
            
        } finally {
              try {
                  fos.close();
              } catch (IOException ex) {
                  ;
              } 
          }
      }
      
      public static void load() {
          FileInputStream fis = null;
          try {
              File file = new File("User.dat");
              fis = new FileInputStream(file);
              ObjectInputStream ois = new ObjectInputStream(fis);
              ArrayList<User> user = (ArrayList<User>) ois.readObject();
              UserService.addUser(user);
              
              ois.close();
              fis.close();
              
          } catch (FileNotFoundException ex) {
          
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
           
        } finally {
              try {
                  fis.close();
              } catch (IOException ex) {
                  
              }
          }
      }
      
}
